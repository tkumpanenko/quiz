(function($){

  var data = {
    questions : [
    {
      idx : 0, 
      image_src : 'slide-00.png',
      description : 'Shivam and Shrey are playing in their garden',
      question : 'How was the interaction between the boys?',
      target: 0,
      answers: [
      {idx: 0, text:'Normal interaction because the boys are playing without hurting anyone. This is called Kraman.'},
      {idx: 1, text:'Wrong conduct that hurt Shivam. This is called Atikraman.'},
      {idx: 2, text:'Shrey and Shivam should not play together.'}
      ]
    },
    {
      idx : 1, 
      question : 'Is Varun’s attitude correct towards his teacher?',
      image_src : 'slide-01.png',
      description : 'Varun doesn’t like Maths. He feels that the maths teacher tortures him.',
      target: 1,
      answers: [
      {idx: 0, text:'Yes. Because Varun dislikes Maths.'},
      {idx: 1, text:'Wrong. Because Varun has found fault with his teacher.'},
      {idx: 2, text:'Neither right nor wrong because no one knows Varun’s thoughts.'}
      ]
    },
    {
      idx : 2, 
      question : 'What should he do next?',
      target: 1,
      image_src : 'slide-01.png',
      answers: [
      {idx: 0, text:'Varun should avoid studying Maths since he doesn’t like the subject.'},
      {idx: 1, text:'He should ask for forgiveness and resolve never to see the teacher’s faults again. This is called Pratikraman.'},
      {idx: 2, text:'He should change the school so that he doesn’t encounter that teacher again.'}
      ]
    },
    {
      idx : 3, 
      question : 'Should Amit and Rupak ask for Rohan’s forgiveness (do pratikraman) for calling him a ‘dumbo’?',
      image_src : 'slide-03.png',
      description : 'Rohan failed the science test, whereas Amit and Rupak passed the test with A+. Both Amit and Rupak teased Rohan.',
      target: 1,
      answers: [
      {idx: 0, text:'No, because there is nothing wrong in that.'},
      {idx: 1, text:'Yes, because their actions hurt Rohan.'},
      {idx: 2, text:'Yes, because their actions were visible otherwise there was no need.'}
      ]
    },
    {
      idx : 4, 
      question : 'The above situation is:',
      image_src : 'slide-04.png',
      description : 'Montu has made a rule that Pintoo should always obey him. Otherwise, Montu starts his verbal bullying........',
      target: 1,
      answers: [
      {idx: 0, text:'Kraman because it is a normal behavior as Montu has not physically hurt Pintoo.'},
      {idx: 1, text:'Atikraman because Montu has hurt Pintoo with his harsh speech.'},
      {idx: 2, text:'Neither Kraman nor Atikraman because words never hurt.'}
      ]
    },
    {
      idx : 5, 
      question : 'What should Montu do next?',
      target: 0,
      image_src : 'slide-04.png',
      answers: [
      {idx: 0, text:'Immediately do Pratikraman and become friends with Pintoo.'},
      {idx: 1, text:'Wait for at least twelve months before he does Pratikraman.'},
      {idx: 2, text:'He should not play with Pintoo anymore.'}
      ]
    },
    {
      idx : 6,
      description: 'Hetal’s friends gave a surprise party on her birthday',
      question : 'Have Hetal’s friends hurt her? Should they do Pratikraman for giving a party? ',
      image_src : 'slide-05.png',
      target: 0,
      answers: [
      {idx: 0, text:'No, because there is nothing wrong with having innocent fun that delights all.'},
      {idx: 1, text:'Yes, because having fun is a waste of time and energy.'},
      {idx: 2, text:'Yes, because surprises always hurt.'}
      ]
    },
    {
      idx : 7,
      description : 'Mr. Chaganlal is very annoyed with the mosquito that bit him',
      image_src : 'slide-06.png',
      question : 'What should he do?',
      target: 2,
      answers: [
      {idx: 0, text:'Take God’s name and kill it so that it attains a higher life form.'},
      {idx: 1, text:'Use insecticides in his home'},
      {idx: 2, text:'Sleep in a mosquito net and pray that no one is hurt by him.'}
      ]
    }
    ]
  };

  var template = $('#app').html();
  var rendered = Mustache.render(template, data);
  $('#wrapper').html(rendered);


  var quiz = {
    selectors : {
      question : '.question',
      answer : '.question__answers li',
    }, 
    questions : $('.question'),
    step : 0,
    classes : {
      slideLock: 'lock',
      wrong : 'wrong-answer',
      correct : 'correct-answer',
      selected : 'selected-answer',
    },
    run : function(){
      this.step = 0;
      this.show();
      var selectedClass = this.classes.selected;
      var wrongClass = this.classes.wrong;
      var correctClass = this.classes.correct;
      var lockClass = quiz.classes.slideLock;
      var questions = this.questions;

      $('.js--answer').bind('click', clickAnswer );
      $('.js--answer').removeClass(selectedClass)
                      .removeClass(wrongClass)
                      .removeClass(correctClass);
      questions.removeClass(lockClass);
      $('.js--next-question').addClass('disabled');
    },
    show : function(idx) {
      var index = idx || this.step;
      var questions = this.questions;
      questions.hide();
      if (questions.filter('[data-slide-index='+index+']').length == 0) 
        index = 'last';
      questions.filter('[data-slide-index='+index+']').css({'display':'flex'});
    },
    checkAnswer: function(questionId, answerId) {
      var currentQuestion = data.questions[this.step];

      if (answerId == currentQuestion.target) {
        this.correctAnswer(questionId, answerId);
      }else{
        this.wrongAnswer(questionId, answerId);
      }
    }, 
    correctAnswer : function(questionId, answerId) {
      var _ = this;
      var delay = 1000;

      var selectedClass = this.classes.selected;
      var correctClass = this.classes.correct;

      var question = this.questions.filter('[data-slide-index='+questionId+']');
      var answer = question.find(this.selectors.answer + '[data-answer-index='+answerId+']');
      var nextBtn = question.find('.js--next-question');
      var correctAudio = document.getElementById('correct-audio');
      setTimeout(function() {
        correctAudio.play();
        answer.removeClass(selectedClass).addClass(correctClass);
        setTimeout( function() { nextBtn.removeClass('disabled'); }, delay);
      }, delay);
    },
    wrongAnswer : function(questionId, answerId) {
      var _ = this;
      var delay = 1000;
      var currentQuestion = data.questions[this.step];

      var selectedClass = this.classes.selected;
      var wrongClass = this.classes.wrong;
      var correctClass = this.classes.correct;

      var question = this.questions.filter('[data-slide-index='+questionId+']');
      var answer = question.find(this.selectors.answer + '[data-answer-index='+answerId+']');
      var rightAnswer = question.find(this.selectors.answer + '[data-answer-index='+currentQuestion.target+']');
      var nextBtn = question.find('.js--next-question');
      
      var wrongAudio = document.getElementById('wrong-audio');

      setTimeout(function() {
        wrongAudio.play();
        answer.removeClass(selectedClass).addClass(wrongClass);
        rightAnswer.addClass(correctClass);
        setTimeout( function() { nextBtn.removeClass('disabled'); }, delay);
      }, delay);

    },
    nextQuestion : function(){
      var index = this.step;
      var question = this.questions.filter('[data-slide-index='+index+']');

      this.step++;
      this.show();
    },
  }

  $('#wrapper').on('click','.js--next-question', function(ev){
      if (!$(this).hasClass('disabled'))
        quiz.nextQuestion();
      ev.preventDefault();
  });

  $('.js--play').click(function() {
    quiz.run();
  });


  function clickAnswer() {
    var answerId = parseInt(this.dataset.answerIndex);
    var questionId = parseInt($(this).parents(quiz.selectors.question)[0].dataset.slideIndex);
    var selectedClass = quiz.classes.selected;
    var lockClass = quiz.classes.slideLock;
    var question = quiz.questions.filter('[data-slide-index='+questionId+']');
    $(this).addClass(selectedClass);
    question.addClass(lockClass);
    quiz.checkAnswer(questionId, answerId); 
    question.find(quiz.selectors.answer).unbind('click');
    
  }

})(jQuery);